# Pattern
The main idea is to use Saga Orchestration pattern. More info:
https://microservices.io/patterns/data/saga.html

# Technology stack
- RabbitMQ
- MySql
- Redis
- ExpressJS
- VueJS

# High level architecture diagram
![High level architecture](images/hilevel.png "High level architecture")

# Start
- Run `git clone git@bitbucket.org:1337homework/platform.dev.git` and `cd platform.dev`
- Clone remaining repos `...platform.dev$: npm i meta -g && meta git clone` or manually
- Run `platform.dev$: docker-compose up -d`
- Open `localhost:8885` user `demo:demo` or `admin:admin`

