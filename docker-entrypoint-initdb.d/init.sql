CREATE DATABASE IF NOT EXISTS `automation_saga_controller` COLLATE 'utf8_general_ci' ;
GRANT ALL ON `automation_saga_controller`.* TO 'root' ;

CREATE DATABASE IF NOT EXISTS `security_gateway` COLLATE 'utf8_general_ci' ;
GRANT ALL ON `security_gateway`.* TO 'root' ;

CREATE DATABASE IF NOT EXISTS `users` COLLATE 'utf8_general_ci' ;
GRANT ALL ON `users`.* TO 'root' ;
